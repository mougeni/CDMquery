queryID	queryclass	var1	var2	expression	output
1	var_missing	bp_syst	NA	"is.na(var1)"	"systolic blood pressure is missing"
2	var_too_low	age	NA	"as.numeric(var1) < 10"	"age is below 10"
3	var_too_high	temperature	NA	"as.numeric(var1) > 40"	"temperature is above 40"
4	var_not_in_list	sex	NA	"!(var1 %in% c('M','F'))"	"sex not M or F"
<<<<<<< HEAD
5	var_not_in_extern_list	location	NA	"parameter1 %in% as.character(read_csv('externaldata/villages.txt'))"	"Location not in 'villages.txt' file"
=======
5	var_not_in_extern_list	location	NA	"parameter1 %in% as.character(read.delim('externaldata/villages.txt'))"	"Location not in 'villages.txt' file"
>>>>>>> 98cadecd36c94eceaeff9ec51cfc8f5633ae0f24
6	Date_var1_lower_than_date_var2	vis1_dt	scr_dt	"as.Date(var1) < as.Date(var2)"	"Visit1 date before screening date"
7	Date_var1_lower_than_date_var2_formatDate	vis1_dt	scr_dt	"as.Date(var1, '%d/%m/%Y') < as.Date(var2, '%d/%m/%Y')"	"Visit1 date before screening date"
8	evaluate_var2_depends_on_var1	sex	pregnancy	"if(var1 == 'M') !is.na(var2)"	"pregnancy value in male"
