Introduction
============

CDMquery provides a framework for cleaning data. In principle, it can be used for any kind of data, however it has been created with Clinical Data Management (CDM) in mind. During the 'data cleaning' process after a clinical trial has been completed, the validity and consistency of the database entries are checked. The investigator receives a list of flagged data entries. These are called "queries", and they constitute an important part for data management of clinical trials.

For each query, the investigators examine the database against source documents and either perform changes to the database or confirm the value.

Other important aspects of Clinical Data Management include the requirement for good documentation of the cleaning process.

The structure of data collected in a clinical trial and the type of data and items are similar between trials. Queries are therefore typically similar from trial to trial and require minimal adaption. A library of expressions that define query is 

The CDMquery package provides data managers and investigators a simple framework to perform automated queries with only basic knowledge of the R language. It provides documentation of each step. It is important to realise that the system can only identify simple errors - those that can be identified with a simple expression in one dataset. Identification of complex errors, e.g. those arising during statistical analysis after transforming and joining different datasets can currently not be identified with this basic system.

Requirements
-------------
* The data should be available as tab-delimited txt file (more file formats and database connectivity are planned)
* Query definitions contain the expression used to identify suspicious data. They are kept in a tab-delimited txt file named 'querydefinitions' in the 'definitions' folder. The expressions are written in the 'R' language.
* An empty template with the correct structure is put in the folder during the initialisation process.
* A more complete list of examples can be found somewhere. This includes expressions to validate against an external list and comparing dates in non-standard format. Queries fall in a limited amount of classes and it is possible to create the expressions using common sense - no knowledge of 'R' is required. 

##### Example for a querydefinitions file:

| queryID|queryclass                     |var1        |var2   |expression                    |output                             |
|-------:|:------------------------------|:-----------|:------|:-----------------------------|:----------------------------------|
|       1|var_missing                    |bp_syst     |NA     |is.na(var1)                   |systolic blood pressure is missing |
|       2|var_too_low                    |ageyr       |NA     |as.numeric(var1) < 10         |age in years is below 10           |
|       3|var_too_high                   |temperature |NA     |as.numeric(var1) > 40         |temperature is above 40            |
|       4|var_not_in_list                |sex         |NA     |!(var1 %in% c('M','F'))       |sex not M or F                     |
|       5|date_var1_lower_than_date_var2 |vis1_dt     |scr_dt |as.Date(var1) < as.Date(var2) |visit1 date before screening date  |



Workflow
-------
#### Initialising the workspace
1. initialise workspace by running the `initialise()` function. 
    - the `initialise()` function takes only the path to the project folder as parameter.
    - Example: `initialise("C:/Users/fabr/Documents/studies/study_101")`
2. This will add a folder "query" and several sub-folders in your projectfolder.
3. Add query definitions to the file "queryDefinitions.txt" in the "definitions" folder
4. Run `initialise()` again to check if everything is set up correctly. If everything is ok, you will see the message "The working space has been initialised successfully."

#### Running the query
1. Run `query()`. The following parameters need to be specified:
    - projectfolder: the folder used to initialise the workspace
    - file: the name of the datafile. The path is specified relative to projectfolder. i.e. if the datafile is beneath the projectfolder, no path needs to be given. Otherwise specify the whole path.
    - source: for data tables (in contrast to databases) this is either 'tsv' for 'tab delimited value' txt data, or 'csv' for 'comma separated value' txt files
2. The query will run the expressions in the querydefinitions table against the datatable specified in "file".
3. The findings are saved in the table 'queryfindings_current.txt' in the 'findings' folder. The table contains informtion on date and time the query was run. Each finding is shown with the uniqueID specified in the query parameters.
4. Open the 'queryfindings_current.txt' table in a spreadsheet. Findings that are based on an error in the transcription from source data to database need to be corrected in the database.
Findings that are true are confirmed by writing "confirmed" in the output column of the table.
Comments can be added in the 'comment' section.
5. Once all findings have been attended to, the `query()`command is run again.
    - firstly, it stores all 'confirmed' findings in a folder called 'queryconfirmed.txt' in the 'archive' folder.
    - secondly, it will create a new 'queryfindings_current.txt' instead of the previous one
    - thirdly, it appends any new findings to a table 'queryfindings_archive.txt' in the 'archive' folder
6. Continue until there are no more findings.
